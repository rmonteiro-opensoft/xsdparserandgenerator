package ${package}.validator.${jpaClass.substring(0,1).toLowerCase()}${jpaClass.substring(1)};

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;


import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
* Classe gerada pela tool ValidationsGenerator
*/

@Service
public class ${validatorClassName} {

    private static Logger logger = LoggerFactory.getLogger(${jpaClass}Validator.class);

#foreach( $validation in $validations )
    #if($validation.pattern)
    private static final String ${validation.columnName.toUpperCase()}_PATTERN = "${validation.pattern}";
    #end
    #if($validation.enumeration)
    private static final List<String> ${validation.columnName.toUpperCase()}_ENUM = Arrays.asList(
        #foreach( $enum in $validation.enumeration )
            "$enum" #if( $foreach.hasNext ), #end
        #end
    );
    #end
#end

    public ${validatorClassName}() {

    }


    public void validate(${jpaClass}Model model, BindingResult errors)  {
#foreach( $validation in $validations )
    #set( $columnNameVar = ${validation.varName} )
    #set( $columnNameVarCapitalized = $columnNameVar.substring(0,1).toUpperCase() + $columnNameVar.substring(1) )
    #if($validation.pattern
    || $validation.minInclusive
    || $validation.maxInclusive
    || $validation.minExclusive
    || $validation.maxExclusive)
        validate${columnNameVarCapitalized}(model, errors);
    #end
#end
    }

#foreach( $validation in $validations )
    #set( $columnName = ${validation.columnName} )
    #set( $columnNameVar = ${validation.varName} )
    #set( $columnNameVarCapitalized = $columnNameVar.substring(0,1).toUpperCase() + $columnNameVar.substring(1) )
    #set( $modelGetVar = "model.get" + $columnNameVarCapitalized + "()" )

    #if($validation.pattern
    || $validation.enumeration
    || $validation.minInclusive
    || $validation.maxInclusive
    || $validation.minExclusive
    || $validation.maxExclusive
    )
    //$columnName
    private void validate${columnNameVarCapitalized}(${jpaClass}Model model, BindingResult errors){
        #if($validation.pattern)
            GenericValidator.validatePattern("$columnNameVar", $modelGetVar, ${columnName.toUpperCase()}_PATTERN, errors);
        #end
        #if($validation.enumeration)
            GenericValidator.validateEnumeration("$columnNameVar", $modelGetVar, ${columnName.toUpperCase()}_ENUM, errors);
        #end
        #if($validation.dataType == "DATE")
            #set( $minDate = $null )
            #set( $maxDate = $null )
            #set( $isExclusive = false )
            #if($validation.minInclusive)
                #set( $minDate = ${validation.minInclusive} )
            #end
            #if($validation.maxInclusive)
                #set( $maxDate = $validation.maxInclusive )
            #end

            #if($validation.minExclusive)
                #set( $minDate = ${validation.minInclusive} )
                #set( $isExclusive = true )
            #end

            #if($validation.maxExclusive)
                #set( $maxDate = $validation.maxInclusive )
                #set( $isExclusive = true )
            #end
            GenericValidator.validateDate("$columnNameVar", $modelGetVar, "yyyy-mm-dd", $minDate, $maxDate, $isExclusive, ${columnName.toUpperCase()}_ENUM, errors);
        #end
    }
    #end
#end

}
