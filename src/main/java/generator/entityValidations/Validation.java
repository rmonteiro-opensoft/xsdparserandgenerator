package generator.entityValidations;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Validation {



	private String columnName;
	private String varName;
	private String dataType;
	private boolean required;


	private String pattern;
	private String minInclusive;
	private String maxInclusive;
	private String minExclusive;
	private String maxExclusive;

	private List<String> enumeration;

	private String length;
	private String minLength;
	private String maxLength;


	private String totalDigits;

	private String fractionDigits;
	private String whiteSpace;



	public Validation() {

	}

	public Validation(String columnName) {
		this.columnName = columnName;
		generateVarName(columnName);
	}

	private void generateVarName(String columnName){
		this.varName = StringUtils.uncapitalize(Arrays.stream(columnName.split("_"))
				.map(StringUtils::capitalize)
				.collect(Collectors.joining()));

	}

	public String getVarName() {
		return varName;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDataType() {
		return dataType;
	}

	public boolean isRequired() {
		return required;
	}

	public String getColumnName() {
		return columnName;
	}

	public String getPattern() {
		return pattern;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
		generateVarName(columnName);
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public List<String> getEnumeration() {
		return enumeration;
	}

	public void setEnumeration(List<String> enumeration) {
		this.enumeration = enumeration;
	}

	public String getTotalDigits() {
		return totalDigits;
	}

	public void setTotalDigits(String totalDigits) {
		this.totalDigits = totalDigits;
	}

	public String getFractionDigits() {
		return fractionDigits;
	}

	public void setFractionDigits(String fractionDigits) {
		this.fractionDigits = fractionDigits;
	}

	public String getMinInclusive() {
		return minInclusive;
	}

	public void setMinInclusive(String minInclusive) {
		this.minInclusive = minInclusive;
	}

	public String getMaxInclusive() {
		return maxInclusive;
	}

	public void setMaxInclusive(String maxInclusive) {
		this.maxInclusive = maxInclusive;
	}

	public String getMinExclusive() {
		return minExclusive;
	}

	public void setMinExclusive(String minExclusive) {
		this.minExclusive = minExclusive;
	}

	public String getMaxExclusive() {
		return maxExclusive;
	}

	public void setMaxExclusive(String maxExclusive) {
		this.maxExclusive = maxExclusive;
	}

	public String getWhiteSpace() {
		return whiteSpace;
	}

	public void setWhiteSpace(String whiteSpace) {
		this.whiteSpace = whiteSpace;
	}
}
