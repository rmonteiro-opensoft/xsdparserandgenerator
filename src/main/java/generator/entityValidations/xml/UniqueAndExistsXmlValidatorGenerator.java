package generator.entityValidations.xml;

import generator.dsl.generator.NotImplementedException;
import generator.xsd.elements.IdentityConstraint;
import generator.xsd.XsdResult;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class UniqueAndExistsXmlValidatorGenerator {


	private String templatePath = "src\\main\\resources\\templateUniqueAndExistsXmlValidator.vm";
	private File resultDir;
	private String validatorsPackage;

	public UniqueAndExistsXmlValidatorGenerator(String validatorsPackage, File resultDir) {
		this.validatorsPackage = validatorsPackage;
		this.resultDir = resultDir;
	}

	public void generate(XsdResult xsdResult) {

//		List<Element> parentElements = xsdResult.getElementMap().values().stream()
//				.filter(element -> element.getReferencedByCounter() == 0)
//				.collect(Collectors.toList());

		List<IdentityConstraint> identityConstraintList = xsdResult.getIdentityConstraintList();


		// VALIDAÇÕES SÓ PARA AS CONSTRAINTS UNIQUE e KEYREF



		List<IdentityConstraint> uniqueValidations = new LinkedList<>();
		List<IdentityConstraint> existsValidations = new LinkedList<>();

		identityConstraintList.forEach(identityConstraint -> {

			switch (identityConstraint.getType()) {
				case UNIQUE:
					uniqueValidations.add(identityConstraint);
					break;
				case KEYREF:
					existsValidations.add(identityConstraint);
					break;
				case KEY:
					throw new NotImplementedException("KEY validation");
			}
		});

		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.init();
		VelocityContext context = new VelocityContext();
		String className = "Xml";
		context.put("package", validatorsPackage);
		context.put("uniqueValidations", uniqueValidations);
		context.put("existsValidations", existsValidations);

		printVelocityTemplate(templatePath, className, context);

	}


	private void printVelocityTemplate(String templatePath, String className, VelocityContext context) {
		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.init();
		Template t = velocityEngine.getTemplate(templatePath);
		PrintWriter printWriter;
		try {
			printWriter = new PrintWriter(new File(resultDir, className + "Validator.java"));
			t.merge(context, printWriter);
			printWriter.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




}
