package generator.entityValidations;

import java.util.List;

public class Validator {


	private String tableName;
	private List<Validation> validationList;

	public Validator(String tableName, List<Validation> validationList) {
		this.tableName = tableName;
		this.validationList = validationList;
	}

	public String getTableName() {
		return tableName;
	}

	public List<Validation> getValidationList() {
		return validationList;
	}

}
