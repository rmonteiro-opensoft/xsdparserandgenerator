package generator.entityValidations;

import generator.xsd.XsdResult;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

public class GeneratorEntityValidations {


	private String validatorsPackage;
	private File resultDir;
	private String templatePath = "src\\main\\resources\\template.vm";
	private String genericValidatorPath = "src\\main\\resources\\templateGenericValidator.vm";


	public GeneratorEntityValidations(String validatorsPackage, File resultDir) {
		this.validatorsPackage = validatorsPackage;
		this.resultDir = resultDir;
	}

	public void generate(XsdResult xsdResult) {
		List<Validator> validators = xsdResult.getElementMap().values().stream()
				.filter(element -> element.getComplexType() != null)
				.map(ValidationServiceMapper::createValidator)
				.filter(this::hasValidValidations)
				.collect(Collectors.toList());

		validators.forEach(this::generateValidationClass);

		//GenericValidator
		VelocityContext context = new VelocityContext();
		context.put("package", validatorsPackage);
		String validatorClassName = "GenericValidator";
		context.put("validatorClassName", validatorClassName);
		printVelocityTemplate(genericValidatorPath, validatorClassName, context);

	}


	private void printVelocityTemplate(String templatePath, String validatorClassName, VelocityContext context) {
		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.init();
		Template t = velocityEngine.getTemplate(templatePath);
		PrintWriter printWriter;
		try {
			printWriter = new PrintWriter(new File(resultDir, validatorClassName+ ".java"));
			t.merge(context, printWriter);
			printWriter.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void generateValidationClass(Validator validator) {
		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.init();
		VelocityContext context = new VelocityContext();
		String className = validator.getTableName();
		String validatorClassName = className + "XSDValidator";
		context.put("package", validatorsPackage);
		context.put("jpaClass", className);
		context.put("validatorClassName", validatorClassName);
		context.put("validations", validator.getValidationList());

		printVelocityTemplate(templatePath, validatorClassName, context);
	}



	//criar classes só com validações que o template tem - por exemplo o Validator pode ter validações só para o MaxSize, isso não está no template
	private boolean hasValidValidations(Validator validator) {
		List<Validation> validationList = validator.getValidationList();
		return ! validationList.isEmpty()
				&& (validationList.stream().anyMatch(validation ->
				validation.getPattern() != null ||
				validation.getMinInclusive() != null ||
				validation.getMaxInclusive() != null ||
				validation.getMinExclusive() != null ||
				validation.getMaxExclusive() != null));
	}


}
