package generator.entityValidations;

import generator.dsl.generator.NotImplementedException;
import generator.xsd.elements.ComplexType;
import generator.xsd.elements.Element;
import generator.xsd.elements.SimpleType;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ValidationServiceMapper {



	public static Validator createValidator(ComplexType complexType) {
		List<Validation> elementValidations = createValidations(complexType.getElementList());
		return new Validator(complexType.getName(), elementValidations);
	}

	public static Validator createValidator(Element element) {
		List<Validation> elementValidations = createValidations(element.getComplexType().getElementList());
		return new Validator(element.getName(), elementValidations);
	}



	public static List<Validation> createValidations(List<Element> elementList) {
		return elementList.stream()
				.map(ValidationServiceMapper::createValidation)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	public static Validation createValidation(Element element) {
		Validation validation = new Validation();

		if (element.getName() != null) {
			validation.setColumnName(element.getName());
		}

		if (! BigInteger.ZERO.equals(element.getMinOccurs()) ) {
			validation.setRequired(true);
		}


		if (! BigInteger.ONE.equals(element.getMaxOccurs()) ) {
			if (BigInteger.ONE.negate().equals(element.getMaxOccurs())) { 	// array
				//				throw new NotImplementedException("array");
//					System.out.println(element.getRef().getName() + " - " + element.getMinOccurs());
			}
			else { 		//array com limite
//				throw new NotImplementedException("array");
			}
		}

		SimpleType simpleType;
		if (element.getSimpleType() != null) {
			simpleType = element.getSimpleType();
			ValidationServiceMapper.setValidations(validation, simpleType);
//			throw new NotImplementedException("simpleType not null");

			return validation;
		}

		ComplexType complexType = element.getComplexType();
		if (complexType != null) {
//			ValidationServiceMapper.setValidations(validation, simpleType);
			return validation;

		}

		Element ref = element.getRef();
		if (ref != null) {
			SimpleType refSimpleType = ref.getSimpleType();
			validation.setColumnName(ref.getName());

			if (refSimpleType != null) {
				ValidationServiceMapper.setValidations(validation, refSimpleType);
			}
			else {
				//nao cria validações aqui - será processado independentemente
			}
		}
		else {
			if (! element.getSequenceElements().isEmpty()) {
				element.getSequenceElements().forEach(sequenceElement -> {

					SimpleType sequenceElementSimpleType = sequenceElement.getSimpleType();
					if (sequenceElementSimpleType == null) {
						Element ref1 = sequenceElement.getRef();
						if (ref1 != null) {

							validation.setColumnName(ref1.getName());
							sequenceElementSimpleType = ref1.getSimpleType();
						}
						else{
							throw new NotImplementedException("ref = null");
						}
					}
					if (sequenceElementSimpleType != null) {
						ValidationServiceMapper.setValidations(validation, sequenceElementSimpleType);
					}
					else {
						throw new NotImplementedException("sequenceElementSimpleType == null");
					}
				});
			}
			else {
//				throw new NotImplementedException("element.getSequenceElements().isEmpty()");
				return null;
			}
		}

		if (validation.getColumnName() == null) {
			return null;
		}
//		assert validation.getColumnName() != null;
		return validation;
	}

	public static void setValidations(Validation validation, SimpleType simpleType) {
		validation.setDataType(simpleType.getBaseType());
		validation.setLength(simpleType.getLength());
		validation.setMinLength(simpleType.getMinLength());
		validation.setMaxLength(simpleType.getMaxLength());
		validation.setPattern(simpleType.getPattern());
		validation.setEnumeration(simpleType.getEnumeration());
		validation.setTotalDigits(simpleType.getTotalDigits());
		validation.setFractionDigits(simpleType.getFractionDigits());
		validation.setMinInclusive(simpleType.getMinInclusive());
		validation.setMaxInclusive(simpleType.getMaxInclusive());
		validation.setMinExclusive(simpleType.getMinExclusive());
		validation.setMaxExclusive(simpleType.getMaxExclusive());
		validation.setWhiteSpace(simpleType.getWhiteSpace());
	}
}
