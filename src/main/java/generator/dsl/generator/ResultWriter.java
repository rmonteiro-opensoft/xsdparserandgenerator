package generator.dsl.generator;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class ResultWriter {



	public static void writeResultFiles(File resultDir, List<EntityModel> entities)  {
		final File resultDirFinal = resultDir;
		resultDirFinal.mkdirs();

		entities.forEach(entity -> {
			File file = new File(resultDirFinal, entity.getName() + ".entity");

			try {
				Files.write(file.toPath(), entity.getContent().getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
			} catch (IOException e) {
				e.printStackTrace();
			}


		});

	}

}
