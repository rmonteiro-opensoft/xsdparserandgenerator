package generator.dsl.generator;

import generator.xsd.elements.ComplexType;
import generator.xsd.elements.Element;
import generator.xsd.elements.SimpleType;
import generator.xsd.reader.services.ElementService;

import java.io.File;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class GeneratorDSL {

	private File resultDir;

	public GeneratorDSL(File resultDir) {
		this.resultDir = resultDir;
	}

	private static Queue<Element> queue = new LinkedList<>();
	private static Set<String> printed = new HashSet<>();


	private static final boolean TEST = true;
	private static final boolean PRINT_EXTENSIVE_DSL = false;
	private static final boolean VALID_FOR_TELOSYS = true;

	private static boolean isUnbounded(BigInteger value){
		if (value != null) {
			return BigInteger.ONE.equals(value.negate());		//No xsom o valor é colocado a -1 quando é unbounded no xsd
		}
		return false;
	}

	private static List<String> getDslValidations(Element element) {
		List<String> validations = new LinkedList<>();
		if (BigInteger.ONE.equals(element.getMinOccurs()) && BigInteger.ONE.equals(element.getMaxOccurs())) {
			validations.add("@NotNull");
		}
		else if (BigInteger.ONE.equals(element.getMinOccurs()) && isUnbounded(element.getMaxOccurs())){
					//TODO[RACM]
		}


		return validations;
	}

	private static void generateCatalogTable(String tableName, List<String> options){
		throw new NotImplementedException("generateCatalogTable");
	}

	private static List<String> getDslValidations(SimpleType simpleType, Element element) {
		List<String> validations = new LinkedList<>();


		if (simpleType.getMinLength() != null) {
			validations.add("@SizeMin("+simpleType.getMinLength()+")");
		}
		if (simpleType.getMaxLength() != null) {
			validations.add("@SizeMax("+simpleType.getMaxLength()+")");
		}

		if (simpleType.getLength() != null) {
			validations.add("@SizeMin("+simpleType.getLength()+")");
			validations.add("@SizeMax("+simpleType.getLength()+")");
		}
		if (simpleType.getPattern() != null) {
			if (! VALID_FOR_TELOSYS) {
				validations.add("@Pattern("+simpleType.getPattern()+")");
				if(!TEST) {throw new NotImplementedException("Validation Pattern");}
			}
		}
		if (simpleType.getEnumeration() != null) {
			if (! VALID_FOR_TELOSYS) {
				validations.add("@Enumeration("+simpleType.getEnumeration()+")");
				generateCatalogTable(element.getName(), simpleType.getEnumeration());
				if(!TEST) {throw new NotImplementedException("Validation Enumeration");}
			}
		}
		if (simpleType.getTotalDigits() != null) {
			if (! VALID_FOR_TELOSYS) {
				validations.add("@TotalDigits("+simpleType.getTotalDigits()+")");
				if(!TEST) {throw new NotImplementedException("Validation TotalDigits");}
			}
		}
		if (simpleType.getFractionDigits() != null) {
			if (! VALID_FOR_TELOSYS) {
				validations.add("@FractionDigits("+simpleType.getFractionDigits()+")");
				if(!TEST) {throw new NotImplementedException("Validation FractionDigits");}
			}
		}
		if (VALID_FOR_TELOSYS && simpleType.isNumeric() || ! VALID_FOR_TELOSYS) {	// Telosys apenas suporta tipos numericos
			if (simpleType.getMinInclusive() != null) {
				validations.add("@Min("+simpleType.getMinInclusive()+")");
				if(!TEST) {throw new NotImplementedException("Validation getMinInclusive");}
			}
			if (simpleType.getMaxInclusive() != null) {
				validations.add("@Max("+simpleType.getMaxInclusive()+")");
				if(!TEST) {throw new NotImplementedException("Validation getMaxInclusive");}
			}
			if (simpleType.getMinExclusive() != null) {
				validations.add("@Min("+simpleType.getMinExclusive()+")");		// Para o DSL metemos @Min, aceita o valor, não é esxclusivo, mas depois temos validações para excluir o valor
				if(!TEST) {throw new NotImplementedException("Validation getMinExclusive");}
			}
			if (simpleType.getMaxExclusive() != null) {
				validations.add("@Max("+simpleType.getMaxExclusive()+")");	// Para o DSL metemos @Max, aceita o valor, não é esxclusivo, mas depois temos validações para excluir o valor
				if(!TEST) {throw new NotImplementedException("Validation getMaxExclusive");}
			}
		}

		if (simpleType.getWhiteSpace() != null) {
			if (! VALID_FOR_TELOSYS) {
				validations.add("@WhiteSpace("+simpleType.getWhiteSpace()+")");
				if(!TEST) {throw new NotImplementedException("Validation WhiteSpace");}
			}
		}

		return validations;
	}


	private static boolean elementIsEmptyEntity(Element element) {
		ComplexType complexType = element.getComplexType();

		AtomicBoolean allChildsAreArrays = new AtomicBoolean(true);

		complexType.getElementList()
				.forEach(el -> {
					if(! isArray(el)) {
						allChildsAreArrays.set(false);
					}
				});

		return allChildsAreArrays.get();
	}

	private static void printParentId(Element parentFromMap, Element element, StringBuilder dsl){
		boolean printParentId = false;
		Element parent = element.getParent();


		Element elementToPrint = parent;
		if (parentFromMap != null ) {
			elementToPrint = parentFromMap;
			printParentId = true;
		}
		else {
			if (parent != null) {
				Element parentRef = parent.getRef();
				if (parentRef != null) {
					throw new NotImplementedException("parentRef != null");
				}
				ComplexType complexType = parent.getComplexType();
				if (complexType != null) {
					printParentId = complexType.getElementList().stream()
							.filter(el -> el.equals(element) || element.equals(el.getRef()))
							.findFirst()
							.map(GeneratorDSL::isArray)
							.orElseThrow(RuntimeException::new);
				}

				if (printParentId) {
					while (elementIsEmptyEntity(elementToPrint)) {
						elementToPrint = elementToPrint.getParent();
					}
				}
			}
		}

		if (printParentId) {
			String parentName = elementToPrint.getName();
			dsl.append("\t ")
					.append(decapitalize(parentName)).append("Id : ").append(parentName)
					.append(" { @NotNull } ; \t//parent table \n");
		}


	}

	private static boolean isArray(Element element){
		return isUnbounded(element.getMaxOccurs()) || BigInteger.ONE.compareTo(element.getMaxOccurs()) < 0;
	}

	private static String printArray(Element element){
		if (isArray(element)) {
			return "[]";
		}
		return "";
	}

	private static String getType(SimpleType simpleType) {
		if (VALID_FOR_TELOSYS) {
			switch (simpleType.getBaseType()) {
				case "dateTime":
					return "date";

				case "anySimpleType":
					return simpleType.getName();

				case "gYearMonth":	//TODO devoler o que?
					return simpleType.getName();

				default:
					return simpleType.getBaseType();
			}
		}

		return simpleType.getBaseType();
	}


	private static void printFieldLine(Element parent, Element element, StringBuilder dsl) {
		if (element.getName() == null) {
			if (element.getRef() != null) {
				queue.add(element.getRef());

				if (element.getRef().getSimpleType() != null && ! PRINT_EXTENSIVE_DSL) {
					SimpleType simpleType = element.getRef().getSimpleType();
					List<String> validations = getDslValidations(simpleType, element.getRef());
					validations.addAll(getDslValidations(element));
					if (! isArray(element)) {
						dsl.append(printFieldLine(element.getRef().getName(), getType(simpleType) + printArray(element), validations));
					}
				}
				else {
					if (element.getRef().getSimpleType() == null && element.getRef().getComplexType() == null) {
						if (VALID_FOR_TELOSYS) {
							dsl.append(printFieldLine(element.getRef().getName(), "string", Collections.emptyList()));
						}
						else {
							dsl.append(printFieldLine(element.getRef().getName(), "\"" + element.getRef().getFixedValue() + "\"", Collections.emptyList()));
						}
					}
					else {
						List<String> validations = getDslValidations(element);
						if (! isArray(element)) {
							dsl.append(printFieldLine(element.getRef().getName(), element.getRef().getName() + printArray(element), validations));
						}
					}


				}
			}
			else {
				printFields(parent, element.getSequenceElements(), dsl);
			}
		} else {

			if (! PRINT_EXTENSIVE_DSL && element.isSimpleType()) {

						//TODO[RACM]

				List<String> validations = getDslValidations(element.getSimpleType(), element);
				dsl.append(printFieldLine(element.getName(), getType(element.getSimpleType()), validations));




			}
			else {
				queue.add(element);
				if (element.getMaxOccurs().compareTo(BigInteger.ONE) > 0) {
					ElementParentsMap.add(element, parent);
//				throw new NotImplementedException("Este elemento deve apontar para o pai.");
				}
				else {
					dsl.append(printFieldLine(element.getName(), element.getName(), Collections.emptyList()));
				}
			}

		}
	}

	static class ElementParentsMap {
		private static Map<String, List<Element>> elementParentsMap = new HashMap<>();

		static List<Element> getParents(Element element) {
			List<Element> parentsList = elementParentsMap.get(element.getName());
			if (parentsList == null) {
				return Collections.emptyList();
			}
			return parentsList;
		}

		static void add(Element element, Element parent) {
			List<Element> list = elementParentsMap.get(element.getName());
			if (list == null) {
				list = new LinkedList<>();
			}
			list.add(parent);
			elementParentsMap.put(element.getName(), list);
		}
	}


	private static String typeNameConversion(String type) {
		if ("integer".equals(type.trim())) {
			return "int";
		}
		return type;
	}

	private static String decapitalize(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}

		char chars[] = s.toCharArray();
		chars[0] = Character.toLowerCase(chars[0]);
		return new String(chars);
	}

	private static String printFieldLine(String name, String type, List<String> validations) {
		if (validations.isEmpty()) {
			return MessageFormat.format("\t {0} : {1} ; \n", decapitalize(name), typeNameConversion(type));
		}
		return MessageFormat.format("\t {0} : {1} '{' {2} '}' ; \n", decapitalize(name), typeNameConversion(type), String.join(", ", validations));

	}

	private static void printFields(Element parent, List<Element> elements, StringBuilder dsl) {
		for (Element element : elements) {
			if (element.getComplexType() != null && element.getName() == null) {
				for (Element childElement : element.getComplexType().getElementList()) {
					printFields(parent, childElement.getComplexType().getElementList(), dsl);
				}
			} else {
				printFieldLine(parent, element, dsl);
			}
		}
	}

	public void generateDSL() {
		Map<String, Element> elementMap = ElementService.getElementMap();

		List<Element> parentElements = elementMap.values()
				.stream()
				.filter(element -> element.getChildrenCounter() == 0)
				.collect(Collectors.toList());

		queue.addAll(parentElements);

		List<EntityModel> entityModelList = new LinkedList<>();
		while (! queue.isEmpty()) {
			Element element = queue.poll();
			if (printed.contains(element.getName())
					|| (element.getSimpleType() != null && ! PRINT_EXTENSIVE_DSL)
					|| (element.getComplexType() == null && element.getSimpleType() == null && element.getFixedValue() != null)) {		//elementos de simpleType não são para gerar DSL
				continue;
			}

			List<Element> parents = ElementParentsMap.getParents(element);
			if (! parents.isEmpty()) {
				parents.forEach(p -> {

					if (p.getChildrenCounter() == 1) {	//O pai só tem este filho, então a entity DSL pode referenciar o avô
						Element grandparent = ElementService.findByComplexTypeName(p.getPath().replace("/" + p.getName(), "").replace("/", ""));
						printElement(grandparent, entityModelList, element);
					}
					else {
						printElement(p, entityModelList, element);
					}


				});
			}
			else {
				printElement(null, entityModelList, element);
			}




			printed.add(element.getName());

		}

		ResultWriter.writeResultFiles(resultDir, entityModelList);

	}

	private void printElement(Element parent, List<EntityModel> entityModelList, Element element) {
		StringBuilder dsl = new StringBuilder();
//		String entityName = (parent != null ? parent.getName() : "") + element.getName();
		String entityName = element.getName();
		dsl.append(entityName);
		dsl.append("{ ").append(System.lineSeparator());

		dsl.append("\t id : long { @Id, @NotNull } ; \n");

		printParentId(parent, element, dsl);


		if (element.getComplexType() != null) {
			printFields(element, element.getComplexType().getElementList(), dsl);
		}
		else if(element.getSimpleType() != null && PRINT_EXTENSIVE_DSL){
			List<String> validations = getDslValidations(element.getSimpleType(), element);
			dsl.append(printFieldLine(element.getSimpleType().getName(), element.getSimpleType().getBaseType(), validations));
		}
		else if(element.getFixedValue() != null && PRINT_EXTENSIVE_DSL){
			dsl.append(printFieldLine(element.getName(), "\"" + element.getFixedValue() + "\"", Collections.emptyList()));
		}
		else {
			throw  new NotImplementedException("print element");
		}

		dsl.append("}").append(System.lineSeparator());

		EntityModel entityModel = new EntityModel(entityName, dsl.toString());

		String entityString = dsl.toString();
		if (! entityString.contains("\t id : long { @Id, @NotNull } ; \n}")) {	// criar entity só se tem mais fields que apenas o id
			entityModelList.add(entityModel);
		}
	}


}
