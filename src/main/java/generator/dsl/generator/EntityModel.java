package generator.dsl.generator;

public class EntityModel {


	private String name;
	private String content;

	public EntityModel(String name, String content) {
		this.name = name;
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public String getContent() {
		return content;
	}
}
