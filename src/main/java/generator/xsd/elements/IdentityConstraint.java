package generator.xsd.elements;

public class IdentityConstraint {

	public enum IdentityConstraintEnum {
		KEY,
		KEYREF,
		UNIQUE
	}

	private IdentityConstraintEnum type;
	private String name;
	private String selector;
	private String fieldName;

	private IdentityConstraint referencedConstraint;
	private Element element;

	public IdentityConstraint(IdentityConstraintEnum type, String name, String selector, String fieldName, IdentityConstraint referencedConstraint, Element element) {
		this.type = type;
		this.name = name;
		this.selector = removeXsdComponentsFromString(selector);
		this.fieldName = removeXsdComponentsFromString(fieldName);
		this.referencedConstraint = referencedConstraint;
		this.element = element;
	}


	private String removeXsdComponentsFromString(String s){
		return s.replace("ns:", "");
	}

	public IdentityConstraint getReferencedConstraint() {
		return referencedConstraint;
	}

	public String getReferencedConstraintName() {
		if (referencedConstraint != null) {
			return referencedConstraint.name;
		}
		return null;
	}

	public void setType(IdentityConstraintEnum type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSelector(String selector) {
		this.selector = selector;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public IdentityConstraintEnum getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getSelector() {
		return selector;
	}


	public String getFieldName() {
		return fieldName;
	}

	public String getPath(){
		return selector + "/" + fieldName;
	}

	public Element getElement() {
		return element;
	}
}
