package generator.xsd.elements;


import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SimpleType {


	private String baseType;
	private String name;

	private String length;
	private String minLength;
	private String maxLength;
	private String pattern;
	private List<String> enumeration;
	private String totalDigits;
	private String fractionDigits;
	private String minInclusive;
	private String maxInclusive;
	private String minExclusive;
	private String maxExclusive;
	private String whiteSpace;

	private boolean isLocal; //anonymous simple type

	public SimpleType() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBaseType() {
		return baseType;
	}

	public void setBaseType(String baseType) {
		this.baseType = baseType;
	}


	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public List<String> getEnumeration() {
		return enumeration;
	}

	public void setEnumeration(List<String> enumeration) {
		this.enumeration = enumeration;
	}

	public String getTotalDigits() {
		return totalDigits;
	}

	public void setTotalDigits(String totalDigits) {
		this.totalDigits = totalDigits;
	}

	public String getFractionDigits() {
		return fractionDigits;
	}

	public void setFractionDigits(String fractionDigits) {
		this.fractionDigits = fractionDigits;
	}

	public String getMinInclusive() {
		return minInclusive;
	}

	public void setMinInclusive(String minInclusive) {
		this.minInclusive = minInclusive;
	}

	public String getMaxInclusive() {
		return maxInclusive;
	}

	public void setMaxInclusive(String maxInclusive) {
		this.maxInclusive = maxInclusive;
	}

	public String getMinExclusive() {
		return minExclusive;
	}

	public void setMinExclusive(String minExclusive) {
		this.minExclusive = minExclusive;
	}

	public String getMaxExclusive() {
		return maxExclusive;
	}

	public void setMaxExclusive(String maxExclusive) {
		this.maxExclusive = maxExclusive;
	}

	public String getWhiteSpace() {
		return whiteSpace;
	}

	public void setWhiteSpace(String whiteSpace) {
		this.whiteSpace = whiteSpace;
	}

	public void validation(Map<String, String> mapper) {

		this.getClass().getFields().toString();
		mapper.forEach((s, s2) -> {



		});

	}


	public String dsl() {
		String typeAndRestrictions = baseType;

		List<String> restrictions = new LinkedList<>();

		if (getLength() != null) {
			restrictions.add(MessageFormat.format("", getLength()));
		}
		if (getMinLength() != null) {
			restrictions.add(MessageFormat.format("@SizeMin({0})", getMinLength()));
		}
		if (getMaxLength() != null) {
			restrictions.add(MessageFormat.format("@SizeMax({0})", getMaxLength()));
		}
		if (getPattern() != null) {
			restrictions.add(MessageFormat.format("", getPattern()));
		}
		if (getEnumeration() != null) {
			restrictions.add(MessageFormat.format("", getEnumeration()));
		}
		if (getTotalDigits() != null) {
			restrictions.add(MessageFormat.format("", getTotalDigits()));
		}
		if (getFractionDigits() != null) {
			restrictions.add(MessageFormat.format("", getFractionDigits()));
		}
		if (getMinInclusive() != null) {
			restrictions.add(MessageFormat.format("", getMinInclusive()));
		}
		if (getMaxInclusive() != null) {
			restrictions.add(MessageFormat.format("", getMaxInclusive()));
		}
		if (getMinExclusive() != null) {
			restrictions.add(MessageFormat.format("", getMinExclusive()));
		}
		if (getMaxExclusive() != null) {
			restrictions.add(MessageFormat.format("", getMaxExclusive()));
		}
		if (getWhiteSpace() != null) {
			restrictions.add(MessageFormat.format("", getWhiteSpace()));
		}


		if (! restrictions.isEmpty()) {
			baseType += "{ ";
			baseType += String.join(", ", restrictions);
			baseType += " }";
		}

		return typeAndRestrictions;
	}


	public boolean isNumeric() {
		try{
			return getBaseType().matches("short|integer|decimal|long|int");
		}catch (Exception e) {
			setBaseType(getName());
			return getName().matches("short|integer|decimal|long|int");
		}
	}
}
