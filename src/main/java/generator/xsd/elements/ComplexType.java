package generator.xsd.elements;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ComplexType {


	private String minOccurs;
	private String maxOccurs;

	private String name;

	private List<Element> elementList;

	public ComplexType() {
		elementList = new LinkedList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public List<Element> getElementList() {
		return elementList;
	}

	public void addElement(Element element) {
		this.elementList.add(element);
		element.incChildrenCounter();
	}

	public String getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(String minOccurs) {
		this.minOccurs = minOccurs;
	}

	public String getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(String maxOccurs) {
		this.maxOccurs = maxOccurs;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof ComplexType) {
				ComplexType other = (ComplexType) obj;
				if (Objects.equals(name, other.getName())) {
					if (Objects.equals(elementList, other.elementList)) {
						return true;
					}
				}
			}
		}
		return false;

	}
}
