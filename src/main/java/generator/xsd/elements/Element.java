package generator.xsd.elements;


import org.apache.commons.collections.ListUtils;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Element {

	private class Sequence{

		private List<Element> elements;

		public Sequence(List<Element> elements) {
			this.elements = elements;
		}

		public List<Element> getElements() {
			return elements;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj != null) {
				if (obj instanceof Sequence) {
					Sequence other = (Sequence) obj;
					if (ListUtils.isEqualList(elements, other.elements)) {
						return true;
					}

				}
			}
			return false;
		}
	}

	private int childrenCounter = 0;


	private String path;
	private Element parent;

	private String auxName; //pode haver elementos repetidos no xsd - este nome é usado quando isso acontece - concatenação do nome do elemento com o nome do pai.

	private String name;
	private String fixedValue;
	private BigInteger minOccurs;
	private BigInteger maxOccurs;
	private SimpleType simpleType;
	private ComplexType complexType;
	private Element ref;
	private Sequence sequence;

	private List<IdentityConstraint> constraintsList = new LinkedList<>();

	private Element() {
	}

	public Element(String path, Element parent) {
		this.path = path;
		this.parent = parent;
	}

	public Element getParent() {
		return parent;
	}

	public String getPath() {
		return path;
	}

	public ComplexType getComplexType() {
		return complexType;
	}

	public void setComplexType(ComplexType complexType) {
		this.complexType = complexType;
	}

	public void setSequence(List<Element> elements) {
		this.sequence = new Sequence(elements);
		elements.forEach(Element::incChildrenCounter);
	}

	public boolean isSequence() {
		return sequence != null;
	}

	public List<Element> getSequenceElements() {
		if (sequence == null) {
			return Collections.emptyList();
		}
		return sequence.getElements();
	}

	public void setAuxName(String auxName) {
		this.auxName = auxName;
	}

	/**
	 * Nome do elemento, ou concatenação do nome do pai com o nome do elemento se houver nomes repetidos no XSD.
	 */
	public String getName() {
		if (auxName != null) {
			return auxName;
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFixedValue() {
		return fixedValue;
	}

	public void setFixedValue(String fixedValue) {
		this.fixedValue = fixedValue;
	}


	public SimpleType getSimpleType() {
		return simpleType;
	}

	public void setSimpleType(SimpleType simpleType) {
		this.simpleType = simpleType;
	}

	public Element getRef() {
		return ref;
	}

	public void setRef(Element ref) {
		this.ref = ref;
		if (ref == null) {
			throw new InvalidElementException("ref is null");
		}
		ref.incChildrenCounter();
	}

	public void incChildrenCounter() {
		++childrenCounter;
	}

	public int getChildrenCounter() {
		return childrenCounter;
	}

	public BigInteger getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(BigInteger minOccurs) {
		this.minOccurs = minOccurs;
	}

	public BigInteger getMaxOccurs() {
		if (maxOccurs == null) {
			return BigInteger.ONE;
		}
		return maxOccurs;
	}

	public void setMaxOccurs(BigInteger maxOccurs) {
		this.maxOccurs = maxOccurs;
	}


	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Element) {
				Element other = (Element) obj;
				if (Objects.equals(name, other.name)) {
					if (Objects.equals(sequence, other.sequence)) {
						if (Objects.equals(complexType, other.complexType)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}


	public List<IdentityConstraint> getConstraintsList() {
		return constraintsList;
	}

	public void addConstraint(IdentityConstraint constraint) {
		constraintsList.add(constraint);
	}


	public boolean isSimpleType() {
		return simpleType != null;
	}
}
