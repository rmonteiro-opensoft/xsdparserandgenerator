package generator.xsd.parser;

import generator.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XsdParser {

	private static Logger logger = LoggerFactory.getLogger(XsdParser.class);

	private static final String OPEN_COMMENT_TAG = "<!--";
	private static final String CLOSE_COMMENT_TAG = "-->";

	private static final String CLOSE_GENERIC_ELEMENT = "/>";

	private String line = "";
	private String currentElementNameOrRef;
	private Deque<String> path = new ArrayDeque<>(10);
	private BufferedReader br;
	private Pattern pattern;
	private Matcher matcher;

	private boolean isComment = false;

	private boolean isElementDecl = false;		//indica se estamos a processar a declaraçao do elemento


	private List<Assert> assertList = new LinkedList<>();

	public List<Assert> getAssertList() {
		return assertList;
	}

	private String getPath() {
		return String.join("/", path);
	}

	public class Assert {
		private String test;
		private String path;

		public Assert(String test, String path) {
			this.test = test;
			this.path = path;
		}

		public String getTest() {
			return test;
		}

		public String getPath() {
			return path;
		}
	}

	private String readAssertionTest() throws FileEndException {
		pattern = Pattern.compile("test=\\\"(.*)\\\"");
		matcher = pattern.matcher(line);

		while (! matcher.find()) {
			// o conteúdo do test do assert pode estar em multiplas linhas
			String oldLine = line;
			readLine();
			line = oldLine.concat(line);
		}
		String assertTest = matcher.group(1);

		Assert xsdAssert = new Assert(assertTest, getPath());

		assertList.add(xsdAssert);

		return "";
	}

	private void readAssertion() throws FileEndException {
		int indexOfOpenAssert = line.indexOf(Main.XsdPrefix.getOpenElement("assert"));
		if (indexOfOpenAssert != -1) {
//			line = line.substring(indexOfOpenAssert);
			substringLineAfterStr(Main.XsdPrefix.getOpenElement("assert"));
			readAssertionTest();

			int indexOfCloseAssert = line.indexOf(CLOSE_GENERIC_ELEMENT);
			if(indexOfCloseAssert != -1) {
				substringLineAfterStr(CLOSE_GENERIC_ELEMENT);
			}
			else {
				indexOfCloseAssert = line.indexOf(Main.XsdPrefix.getCloseElement("assert"));
				if(indexOfCloseAssert != -1) {
					substringLineAfterStr(Main.XsdPrefix.getCloseElement("assert"));
				}
			}
		}
	}

	private void substringLineAfterStr(String str) {
		int indexOfOpenElement = line.indexOf(str);
		line = line.substring(indexOfOpenElement + str.length());
	}

	private void readElement() throws FileEndException {
		int indexOfOpenElement = line.indexOf(Main.XsdPrefix.getOpenElement("element"));
		if (indexOfOpenElement != -1) {
			isElementDecl = true;
			substringLineAfterStr(Main.XsdPrefix.getOpenElement("element"));

			readName();
			while (currentElementNameOrRef == null) {
				readLine();
				readName();
			}

			addPath(currentElementNameOrRef);

			line = line.replaceAll("\\\"[a-z|A-Z|0-9]*\\\"", "");

		}

		int closeElement = line.indexOf("/>");
		if (isElementDecl && closeElement != -1) {
			removeLast();
			isElementDecl = false;
			line = line.substring(closeElement + "/>".length());
			readElement();
		}

		if (! path.isEmpty()) {
			int indexOfCloseElement = line.indexOf(Main.XsdPrefix.getCloseElement("element"));
			if (indexOfCloseElement == -1) {
				//elemento nao fechou
				int indexOfElementDeclaration = line.indexOf(">");
				if (indexOfElementDeclaration != -1 && isElementDecl) {
					//elemento tem subelementos
					substringLineAfterStr(">");
					readAssertion();
					readElement();
					isElementDecl = false;
				} else {
					//declaraçao de elemento continua nao proxima linha
//					readLine();
				}

			} else {
				//elemento terminou
				removeLast();
				isElementDecl = false;
			}
		}

		readAssertion();
	}

	private void readName() {
//		pattern = Pattern.compile("ref=\\\"[a-z|A-Z|0-9]*\\\"|name=\\\"[a-z|A-Z|0-9]*\\\"");
		pattern = Pattern.compile("ref=\"([a-z|A-Z|0-9]*)\"|name=\"([a-z|A-Z|0-9]*)\"");
		matcher = pattern.matcher(line);
		if (matcher.find()){
			currentElementNameOrRef = matcher.group(1);
			if (currentElementNameOrRef == null && matcher.groupCount() == 2) {
				currentElementNameOrRef = matcher.group(2);
			}
		}
	}


	private void addPath(String elementName){
		path.addLast(elementName);
	}

	private void removeLast(){
		path.removeLast();
		currentElementNameOrRef = path.peekLast();
	}

	private String removeMultipleSpaces(String s){
		s = s.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":assert>", Main.XsdPrefix.getCloseElement("assert")+">");
		s = s.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":element>", Main.XsdPrefix.getCloseElement("element")+">");
		s = s.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":complexType>", Main.XsdPrefix.getCloseElement("complexType")+">");
		s = s.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":sequence>", Main.XsdPrefix.getCloseElement("sequence")+">");
		s = s.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":all>", Main.XsdPrefix.getCloseElement("all")+">");
		s = s.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":simpleType>", Main.XsdPrefix.getCloseElement("simpleType")+">");
		return s.replaceAll("\\s{2,}", " ");
	}

	private String readLine() throws FileEndException {
		try {
			line = br.readLine();
			if (line == null) {
				throw new FileEndException();
			}
			line = removeMultipleSpaces(line);
			ignoreComment();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return line;
	}

	private void ignoreComment() throws FileEndException {
		int commentTagIndex = line.indexOf(OPEN_COMMENT_TAG);
		if (commentTagIndex != -1) {
			String beforeComment = line.substring(0, commentTagIndex);

			int closeCommentIndex = line.indexOf(CLOSE_COMMENT_TAG);
			while (closeCommentIndex == -1 ) {
				readLine();
				closeCommentIndex = line.indexOf(CLOSE_COMMENT_TAG);
			}
			String afterComment = line.substring(closeCommentIndex + CLOSE_COMMENT_TAG.length());
			line = beforeComment + afterComment;

		}

	}



	private class FileEndException extends Exception {

	}

	public void parseXsd(File schemaFile ) throws FileNotFoundException {

		FileReader fileReader = new FileReader(schemaFile);
		br = new BufferedReader(fileReader);

		try {
			while (readLine() != null) {
				readElement();
			}
		}catch (FileEndException e) {

		}


	}


}
