package generator.xsd;

import generator.xsd.elements.ComplexType;
import generator.xsd.elements.Element;
import generator.xsd.elements.IdentityConstraint;

import java.util.List;
import java.util.Map;

public class XsdResult {

	private Map<String, Element> elementMap;
	private Map<String, ComplexType> complexTypes;
	private List<IdentityConstraint> identityConstraintList;

	public XsdResult(Map<String, Element> elementMap, Map<String, ComplexType> complexTypes, List<IdentityConstraint> identityConstraintList) {
		this.elementMap = elementMap;
		this.complexTypes = complexTypes;
		this.identityConstraintList = identityConstraintList;
	}

	public Map<String, Element> getElementMap() {
		return elementMap;
	}

	public Map<String, ComplexType> getComplexTypes() {
		return complexTypes;
	}

	public List<IdentityConstraint> getIdentityConstraintList() {
		return identityConstraintList;
	}
}
