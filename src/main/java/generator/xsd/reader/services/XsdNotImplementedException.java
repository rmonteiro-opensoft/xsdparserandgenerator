package generator.xsd.reader.services;

class XsdNotImplementedException extends RuntimeException{

	public XsdNotImplementedException(String message) {
		super(message);
	}


}
