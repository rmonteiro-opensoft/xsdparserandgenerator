package generator.xsd.reader.services;

import com.sun.xml.xsom.XSIdentityConstraint;
import com.sun.xml.xsom.XSXPath;
import generator.xsd.elements.Element;
import generator.xsd.elements.IdentityConstraint;

import java.util.LinkedList;
import java.util.List;

public class ConstraintService {


	private static List<IdentityConstraint> identityConstraintList = new LinkedList<>();


	public static void createConstraints(List<XSIdentityConstraint> constraintsList, Element element) {
		constraintsList.stream()
				.map(constraint -> createConstraint(constraint, element))
				.forEach(identityConstraint -> identityConstraintList.add(identityConstraint));

	}

	private static IdentityConstraint createConstraint(XSIdentityConstraint constraint, Element element) {
		XSXPath selector = constraint.getSelector();
		String selectorValue = element.getPath() + "/"+ selector.getXPath().value;

		List<XSXPath> fields = constraint.getFields();
		if (fields.size() > 1) {
			throw new XsdNotImplementedException("constraint com multiplos fields");
		}

		XSXPath xsxPath = fields.get(0);
		String fieldValue = xsxPath.getXPath().value;

		IdentityConstraint identityConstraint = null;
		switch (constraint.getCategory()) {
			case XSIdentityConstraint.KEY:
				throw new XsdNotImplementedException("XSIdentityConstraint.KEY");

			case XSIdentityConstraint.KEYREF:

				XSIdentityConstraint referencedKey = constraint.getReferencedKey();

//				String referencedKeyName = referencedKey.getName();

				IdentityConstraint referencedConstraint = createConstraint(referencedKey, element);

				identityConstraint = new IdentityConstraint(IdentityConstraint.IdentityConstraintEnum.KEYREF, constraint.getName(), selectorValue, fieldValue, referencedConstraint, element);

				break;

			case XSIdentityConstraint.UNIQUE:
				identityConstraint = new IdentityConstraint(IdentityConstraint.IdentityConstraintEnum.UNIQUE, constraint.getName(), selectorValue, fieldValue, null, element);
				break;

		}

		element.addConstraint(identityConstraint);
		return identityConstraint;
	}

	public static List<IdentityConstraint> getIdentityConstraintList() {
		return identityConstraintList;
	}
}
