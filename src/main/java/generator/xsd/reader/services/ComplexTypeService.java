package generator.xsd.reader.services;

import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSType;
import generator.xsd.elements.ComplexType;
import generator.xsd.elements.Element;
import generator.xsd.elements.SimpleType;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ComplexTypeService {


	private static Map<String, ComplexType> complexTypeMap = new HashMap<>();


	public static ComplexType get(String key) {
		return complexTypeMap.get(key);
	}

	public static void put(String key, ComplexType complexType) {
		if (key == null) {
//			throw new InvalidException("Key is null");
			return;
		}
		if (complexType == null) {
			throw new InvalidException("ComplexType is null");
		}

		complexTypeMap.put(key, complexType);
	}

	public static void parseComplexTypes(Map<String, XSComplexType> complexTypes) {
		complexTypes.forEach((name, xsComplexType) -> {
			ComplexType complexType = createComplexType(xsComplexType, "", null);

		});
	}

	public static ComplexType createComplexType(XSComplexType xsComplexType, String path, Element parent) {
		ComplexType complexType = new ComplexType();
		complexType.setName(xsComplexType.getName());
		if (xsComplexType.getName() != null) {
			path += "/"+xsComplexType.getName();
		}

		XSParticle xsParticle = xsComplexType.getContentType().asParticle();
		complexType.setMinOccurs(xsParticle.getMinOccurs().toString());
		complexType.setMaxOccurs(xsParticle.getMaxOccurs().toString());

		if (xsParticle.getTerm().isModelGroup()) {
			XSModelGroup xsParticles = xsComplexType.getContentType().asParticle().getTerm().asModelGroup();
			XSParticle[] children = xsParticles.getChildren();
			for (XSParticle child : children) {
				Element element = createElement(child, path, parent);
				complexType.addElement(element);
			}

		}
		else if(xsParticle.getTerm().isElementDecl()){
			//TODO[RACM]
			System.out.println("----");
		}
		else {
			System.out.println("----");			//TODO[RACM]
		}

		put(complexType.getName(), complexType);

		return complexType;
	}

	private static Element createElement(XSParticle particle, String path, Element parent) {
		BigInteger minOccurs = particle.getMinOccurs();
		BigInteger maxOccurs = particle.getMaxOccurs();

		Element newElement = new Element(path, parent);
//		newElement.setMinOccurs(minOccurs);
//		newElement.setMaxOccurs(maxOccurs);

		if (particle.getTerm().isElementDecl()) {
			XSElementDecl xsElementDecl = particle.getTerm().asElementDecl();

			Element childElement = null;
			if (xsElementDecl.isGlobal()) {
				childElement = ElementService.get(xsElementDecl.getName());
//				newElement.setRef(childElement);			//TODO[RACM] acho que esta mal - deve ser: newElement = childElement;
				newElement = childElement;
			}
			if (childElement == null) {
				newElement = createElement(xsElementDecl, path, parent);
			}
			newElement.setMinOccurs(minOccurs);
			newElement.setMaxOccurs(maxOccurs);

//			throw new NoImplementedException("createElement");

		}
		else if(particle.getTerm().isModelGroup()){		//xs:sequence dentro de xs:sequence, ou choice
			List<Element> sequenceElements = new LinkedList<>();
			for (XSParticle child : particle.getTerm().asModelGroup().getChildren()) {
				Element element = createElement(child, path, newElement);
				sequenceElements.add(element);
			}
			newElement.setSequence(sequenceElements);
		}
		else {
			throw new XsdNotImplementedException("Children do complexType não é ElementDecl");
		}



		return newElement;
	}

	private static Element createElement(XSElementDecl xsElementDecl, String path, Element parent) {
		String childName = xsElementDecl.getName();
		path += "/"+childName;


		Element newElement = null;
		if (xsElementDecl.isGlobal()) {
			newElement = ElementService.get(childName);
		}
		if (newElement == null) {
			newElement = new Element(path, parent);
			newElement.setName(xsElementDecl.getName());
			XSType type = xsElementDecl.getType();
			if (type.isSimpleType()) {
/*
				SimpleType simpleType = SimpleTypeService.get(type.getName());
				if (simpleType == null) {
					simpleType = new SimpleType();
					simpleType.setName(type.getName());

					SimpleTypeService.put(simpleType.getName(), simpleType);
					throw new XsdNotImplementedException("simple type");
				}
				newElement.setSimpleType(simpleType);
*/

				if (type.getName() == null) {
					SimpleType simpleType = SimpleTypeService.createSimpleType(type.asSimpleType());
					newElement.setSimpleType(simpleType);
				}
				else {
					SimpleType simpleType = SimpleTypeService.get(type.getName());
					if (simpleType == null) {
						simpleType = SimpleTypeService.createSimpleType(type.asSimpleType());

						SimpleTypeService.put(simpleType.getName(), simpleType);
						newElement.setSimpleType(simpleType);
//						throw new XsdNotImplementedException("simple type");
					}
					else {
						newElement.setSimpleType(simpleType);
					}
				}
			}
			else {
				XSComplexType xsComplexType = xsElementDecl.getType().asComplexType();

				if (xsElementDecl.isLocal() && xsElementDecl.isElementDecl() && "anyType".equals(xsComplexType.getName())) {
					XSParticle xsParticle = xsComplexType.getContentType().asParticle();
					newElement.setMinOccurs(xsParticle.getMinOccurs());
					newElement.setMaxOccurs(xsParticle.getMaxOccurs());
					newElement.setFixedValue(xsElementDecl.getFixedValue().toString());
				}
				else {
					ComplexType complexType = get(xsComplexType.getName());
					if (complexType == null) {
						complexType = createComplexType(xsComplexType, path, newElement);
					}
					newElement.setComplexType(complexType);
				}



//					throw new NoImplementedException("not simple type");
			}


			ConstraintService.createConstraints(xsElementDecl.getIdentityConstraints(), newElement);

			ElementService.put(newElement.getName(), newElement);

		}
		else {
			throw new XsdNotImplementedException("newElement not null");
		}

		return newElement;
	}

	public static Map<String, ComplexType> getComplexTypeMap() {
		return complexTypeMap;
	}
}
