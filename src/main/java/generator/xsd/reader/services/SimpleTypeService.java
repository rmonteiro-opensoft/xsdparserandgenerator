package generator.xsd.reader.services;

import com.sun.xml.xsom.XSSimpleType;
import generator.xsd.elements.SimpleType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleTypeService {


	private static Map<String, SimpleType> simpleTypeMap = new HashMap<>();



	public static SimpleType get(String key) {
		return simpleTypeMap.get(key);
	}

	public static void put(String key, SimpleType simpleType) {
		if (key == null) {
			throw new InvalidException("Key is null");
		}
		if (simpleType == null) {
			throw new InvalidException("SimpleType is null");
		}

		simpleTypeMap.put(key, simpleType);
	}

	public static void parseSimpleTypes(Map<String, XSSimpleType> simpleTypes) {
		simpleTypes.forEach((name, xsSimpleType) -> {

			SimpleType simpleType = SimpleTypeService.createSimpleType(xsSimpleType);

			simpleType.setName(name);


			simpleTypeMap.put(name, simpleType);
		});

	}

	public static SimpleType createSimpleType(XSSimpleType xsSimpleType) {
		String baseType = xsSimpleType.getBaseType().getName();

		SimpleType simpleType = new SimpleType();

		if (xsSimpleType.getName() != null) {
			simpleType.setName(xsSimpleType.getName());
		}
		else {
			simpleType.setName(xsSimpleType.getBaseType().getName());
		}

		simpleType.setBaseType(baseType);

		if(xsSimpleType.getFacet("length") != null) {
			simpleType.setLength(xsSimpleType.getFacet("length").getValue().toString());
		}
		if(xsSimpleType.getFacet("minLength") != null) {
			simpleType.setMinLength(xsSimpleType.getFacet("minLength").getValue().toString());
		}
		if(xsSimpleType.getFacet("maxLength") != null) {
			simpleType.setMaxLength(xsSimpleType.getFacet("maxLength").getValue().toString());
		}
		if(xsSimpleType.getFacet("pattern") != null) {
			simpleType.setPattern(xsSimpleType.getFacet("pattern").getValue().toString());
		}
		if(! xsSimpleType.getFacets("enumeration").isEmpty()) {
			List<String> enumerations = new ArrayList<>(xsSimpleType.getFacets("enumeration").size());
			xsSimpleType.getFacets("enumeration").forEach(xsFacet -> enumerations.add(xsFacet.getValue().toString()));

			simpleType.setEnumeration(enumerations);
		}
		if(xsSimpleType.getFacet("totalDigits") != null) {
			simpleType.setTotalDigits(xsSimpleType.getFacet("totalDigits").getValue().toString());
		}
		if(xsSimpleType.getFacet("fractionDigits") != null) {
			simpleType.setFractionDigits(xsSimpleType.getFacet("fractionDigits").getValue().toString());
		}
		if(xsSimpleType.getFacet("minInclusive") != null) {
			simpleType.setMinInclusive(xsSimpleType.getFacet("minInclusive").getValue().toString());
		}
		if(xsSimpleType.getFacet("maxInclusive") != null) {
			simpleType.setMaxInclusive(xsSimpleType.getFacet("maxInclusive").getValue().toString());
		}
		if(xsSimpleType.getFacet("minExclusive") != null) {
			simpleType.setMinExclusive(xsSimpleType.getFacet("minExclusive").getValue().toString());
		}
		if(xsSimpleType.getFacet("maxExclusive") != null) {
			simpleType.setMaxExclusive(xsSimpleType.getFacet("maxExclusive").getValue().toString());
		}
		if(xsSimpleType.getFacet("whiteSpace") != null) {
			simpleType.setWhiteSpace(xsSimpleType.getFacet("whiteSpace").getValue().toString());
		}

		return simpleType;
	}
}
