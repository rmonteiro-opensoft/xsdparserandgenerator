package generator.xsd.reader.services;

import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import generator.xsd.elements.ComplexType;
import generator.xsd.elements.Element;
import generator.xsd.elements.SimpleType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ElementService {


	private static Logger logger = LoggerFactory.getLogger(ElementService.class);

	private static Map<String, Element> elementMap = new HashMap<>();


	public static Element findByComplexTypeName(String name) {
		List<Map.Entry<String, Element>> list = elementMap.entrySet()
				.stream()
				.filter(el -> {
					if (el.getValue().getComplexType() != null) {
						return name.equals(el.getValue().getComplexType().getName());
					}
					return false;
				})
				.collect(Collectors.toList());
		if (list.size() != 1) {
			throw new InvalidException("Tentiva de encontrar um elemnto por nome, mas existem vários");
		}
		return list.get(0).getValue();
	}

	public static Element get(String key) {
		return elementMap.get(key);
	}

	public static void put(String key, Element element) {
		if (key == null) {
			throw new InvalidException("Key is null");
		}
		if (element == null) {
			throw new InvalidException("Element is null");
		}

		if (elementMap.containsKey(key) && ! Objects.equals(element, elementMap.get(key))) {
			String path = element.getPath();
			String parentPath = path.replace("/" + key, ""); // o path termina com o nome do element
			String[] pathSplitted = parentPath.split("/");
			String parentName = pathSplitted[pathSplitted.length - 1];
			element.setAuxName(parentName + key);
			elementMap.put(parentName + key, element);

			//mudar o nome do outro elemento
			Element otherElement = elementMap.get(key);
			path = otherElement.getPath();
			parentPath = path.replace("/" + key, ""); // o path termina com o nome do element
			pathSplitted = parentPath.split("/");
			parentName = pathSplitted[pathSplitted.length - 1];
			otherElement.setAuxName(parentName + key);
			elementMap.put(key, otherElement);	// - a key do mapa nao pode ser "parentName + key" porque quando houver outro elemento com o mesmo nome ser detectado

		}

		elementMap.put(key, element);
	}

	//Processa a declaraçao de elementos simples
	public static void parseElementsSimpleType(Map<String, XSElementDecl> elementDecls) {
		elementDecls.forEach((name, xsElementDecl) -> {

			//se nao é simpleType não processa aqui
			if (xsElementDecl.getType().isSimpleType()) {

				Element element = new Element("", null);
				element.setName(name);
				SimpleType simpleType = SimpleTypeService.get(xsElementDecl.getType().getName());
				if (simpleType == null) {
					simpleType = SimpleTypeService.createSimpleType(xsElementDecl.getType().asSimpleType());
				}
				if (xsElementDecl.getType().getName() != null) {
					simpleType.setName(xsElementDecl.getType().getName());
				}
				element.setSimpleType(simpleType);

				ElementService.put(name, element);
			}

		});
	}

	//Processa a declaraçao de elementos simples
	public static void parseElementsComplexType(Map<String, XSElementDecl> elementDecls) {
		elementDecls.forEach((name, xsElementDecl) -> {

			if (! elementMap.containsKey(name)) {
				if (xsElementDecl.getType().isComplexType()) {
					XSComplexType xsComplexType = xsElementDecl.getType().asComplexType();

					Element element = new Element(name, null);
					element.setName(name);
					ComplexType complexType = ComplexTypeService.get(xsComplexType.getName());
					if (complexType == null) {
						complexType = ComplexTypeService.createComplexType(xsComplexType, name, element);
					}

					element.setComplexType(complexType);
					ElementService.put(name, element);

					ConstraintService.createConstraints(xsElementDecl.getIdentityConstraints(), element);


				}
			}

		});
	}


	public static void testElementMap() {
		elementMap.forEach((s, element) -> {
			if (element.getChildrenCounter() == 0) {
				logger.info("Element " + element.getName() + " não está a ser referenciado em nenhum outro elemento");
			}
		});
	}

	public static Map<String, Element> getElementMap() {
		return elementMap;
	}
}
