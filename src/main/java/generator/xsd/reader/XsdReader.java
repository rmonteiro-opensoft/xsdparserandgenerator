package generator.xsd.reader;

import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSIdentityConstraint;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.parser.SchemaDocument;
import com.sun.xml.xsom.parser.XSOMParser;
import generator.Main;
import generator.xsd.XsdResult;
import generator.xsd.reader.services.ComplexTypeService;
import generator.xsd.reader.services.ConstraintService;
import generator.xsd.reader.services.ElementService;
import generator.xsd.reader.services.SimpleTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class XsdReader {

	private static Logger logger = LoggerFactory.getLogger(XsdReader.class);


	public XsdResult readFile(File schemaFile, String schemaName) throws IOException, SAXException {
		File tmpFileWithoutAsserts = createTmpFileWithoutAsserts(schemaFile);
		XSOMParser parser = new XSOMParser();
		parser.parse(tmpFileWithoutAsserts);
		Set<SchemaDocument> documents = parser.getDocuments();
		XSSchema schema = parser.getResult().getSchema(schemaName);

		tmpFileWithoutAsserts.deleteOnExit();

		return readXSSchema(schema);
	}


	private File createTmpFileWithoutAsserts(File schemaFile) throws IOException {
		FileReader fileReader = new FileReader(schemaFile);
		File tmpFile = new File("temp_"+schemaFile.getName());
		try (BufferedReader br = new BufferedReader(fileReader)) {
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(tmpFile))) {

				boolean assertNotEnded = false;
				for (String line = br.readLine(); line != null; line = br.readLine()) {
					try {
						if (assertNotEnded) {
							int endAssertIndex = line.indexOf("/>");
							if (endAssertIndex != -1) {
								line = line.replace("/>", "/>-->");
								assertNotEnded = false;
							} else {
								continue;
							}
						}

						if (!line.contains(Main.XsdPrefix.getOpenElement("assert"))) {
							bw.write(line);
							bw.write(System.lineSeparator());
							continue;
						}

						if (line.contains("<!--")) {
							if (line.indexOf("<!--") < line.indexOf(Main.XsdPrefix.getOpenElement("assert"))) {
								bw.write(line);
								bw.write(System.lineSeparator());
								continue;
							}
						}

						line = line.replace(Main.XsdPrefix.getOpenElement("assert"), "<!--"+Main.XsdPrefix.getOpenElement("assert"));
						line = line.replaceAll("</[ ]*"+Main.XsdPrefix.getPrefix()+":assert>", Main.XsdPrefix.getCloseElement("assert"));
						if (line.contains(Main.XsdPrefix.getCloseElement("assert"))) {
							line = line.replace(Main.XsdPrefix.getCloseElement("assert"), Main.XsdPrefix.getCloseElement("assert")+"-->");
						} else {
							int beginAssertIndex = line.indexOf("<!--"+Main.XsdPrefix.getOpenElement("assert"));
							String subline = line.substring(0, beginAssertIndex);
							bw.write(subline);
							line = line.substring(beginAssertIndex);

							int endAssertIndex = line.indexOf("/>");
							if (endAssertIndex != -1) {
								line = line.replace("/>", "/>-->");
								bw.write(line);
								bw.write(System.lineSeparator());
							} else {
								assertNotEnded = true;
							}
						}


					} catch (IOException e) {
						logger.error(e.getMessage(), e);
					}
				}
				bw.flush();
			}
		}

		return tmpFile;
	}



	private XsdResult readXSSchema(XSSchema schema) {
		Map<String, XSElementDecl> elementDecls = schema.getElementDecls();

		Map<String, XSSimpleType> simpleTypes = schema.getSimpleTypes();
		Map<String, XSComplexType> complexTypes = schema.getComplexTypes();
		Map<String, XSIdentityConstraint> identityConstraints = schema.getIdentityConstraints();

		XsdResult xsdResult = new XsdResult(ElementService.getElementMap(), ComplexTypeService.getComplexTypeMap(), ConstraintService.getIdentityConstraintList());


		SimpleTypeService.parseSimpleTypes(simpleTypes);

		ElementService.parseElementsSimpleType(elementDecls);

		ComplexTypeService.parseComplexTypes(complexTypes);

		ElementService.parseElementsComplexType(elementDecls);


		ElementService.testElementMap();

		return xsdResult;

	}




}
