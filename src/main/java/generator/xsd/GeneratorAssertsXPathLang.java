package generator.xsd;

import generator.xsd.parser.XsdParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class GeneratorAssertsXPathLang {

    private static Logger logger = LoggerFactory.getLogger(GeneratorAssertsXPathLang.class);

    private File resultDir;

    public GeneratorAssertsXPathLang(File resultDir) {
        this.resultDir = resultDir;
    }


    public void writeAssertsToFile(List<XsdParser.Assert> assertList){

        try {
            try (PrintWriter printWriter = new PrintWriter(new File(resultDir, "Asserts.xpath"))) {
                assertList.forEach(anAssert -> {
                    printWriter.println(anAssert.getPath() + ";");
                    printWriter.println(anAssert.getTest() + ";");
                    printWriter.println("");
                });
            }
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
