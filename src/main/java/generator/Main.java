package generator;


import generator.dsl.generator.GeneratorDSL;
import generator.entityValidations.GeneratorEntityValidations;
import generator.entityValidations.xml.UniqueAndExistsXmlValidatorGenerator;
import generator.xsd.GeneratorAssertsXPathLang;
import generator.xsd.XsdResult;
import generator.xsd.parser.XsdParser;
import generator.xsd.reader.XsdReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;
//import pt.opensoft.generator.GeneratorXsdValidations;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {

	private static Logger logger = LoggerFactory.getLogger(Main.class);


	public static class XsdPrefix {
		private static String prefix = "";
		private static String openPrefix = "";
		private static String closePrefix = "";


		private static void definePrefix(String prefix) {
			XsdPrefix.prefix = prefix;
			openPrefix = "<" + prefix + ":";
			closePrefix = "</" + prefix + ":";
		}

		public static String getPrefix() {
			return prefix;
		}

		public static String getOpenPrefix() {
			return openPrefix;
		}

		public static String getClosePrefix() {
			return closePrefix;
		}

		public static String getOpenElement(String element) {
			return openPrefix + element;
		}

		public static String getCloseElement(String element) {
			return closePrefix + element;
		}

	}

	static class DomDocumentAnalyser {

		private Document document;

		public DomDocumentAnalyser(String filePath) throws IOException, SAXException, ParserConfigurationException {
			this.document = buildDocument(filePath);
		}

		private Document buildDocument(String filePath) throws ParserConfigurationException, IOException, SAXException {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(true);
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			return documentBuilder.parse(new File(filePath));
		}

		private String getNamaspace() {
			NamedNodeMap map = ((Element) document.getDocumentElement()).getAttributes();
			return map.getNamedItem("targetNamespace").getNodeValue();
		}

		private String getPrefix() {
			NamedNodeMap map = ((Element) document.getDocumentElement()).getAttributes();
			for (int i = 0; i < map.getLength(); i++){
				Attr attr = (Attr) map.item(i);
				if ("http://www.w3.org/2001/XMLSchema".equals(attr.getValue())) {
					return attr.getName().split(":")[1];
				}
			}
			return "";
		}


	}

	public static void main(String ...args) throws IOException, SAXException, ParserConfigurationException {

		if (args.length != 3) {
			logger.info("GenerateFromXsd <file> <package>");
		}

		String filePath = args[0];


		DomDocumentAnalyser domDocumentAnalyser = new DomDocumentAnalyser(filePath);

		String packageName = args[1];
		String resultDirPath = args[2];
		String nameSpace = domDocumentAnalyser.getNamaspace();
		XsdPrefix.definePrefix(domDocumentAnalyser.getPrefix());


		File schemaFile = new File(filePath);


		XsdReader xsdReader = new XsdReader();
		XsdResult xsdResult = xsdReader.readFile(schemaFile, nameSpace);


		File resultDir = new File(resultDirPath);
		resultDir.mkdirs();
		File resultDirXml = new File(resultDir, "xml");
		resultDirXml.mkdirs();
		File resultDirEntities = new File(resultDir, "entities");
		resultDirEntities.mkdirs();

		File resultDirDsl = new File(resultDir, "dsl");
		resultDirDsl.mkdirs();

		UniqueAndExistsXmlValidatorGenerator uniqueAndExistsXmlValidatorGenerator = new UniqueAndExistsXmlValidatorGenerator(packageName, resultDirXml);
		uniqueAndExistsXmlValidatorGenerator.generate(xsdResult);


		GeneratorEntityValidations validatorGenerator = new GeneratorEntityValidations(packageName, resultDirEntities);
		validatorGenerator.generate(xsdResult);

		// resultDirDsl = new File(resultDir, "C:\\eclipse-workspace\\TelosysTest\\TelosysTools\\AuditFileModel_model");
		GeneratorDSL dSLGenerator = new GeneratorDSL(resultDirDsl);
		dSLGenerator.generateDSL();


		File resultDirAssert = new File(resultDir, "xsd-asserts");
		resultDirAssert.mkdirs();

		XsdParser xsdParser = new XsdParser();
		xsdParser.parseXsd(schemaFile);
		List<XsdParser.Assert> assertList = xsdParser.getAssertList();

		GeneratorAssertsXPathLang assertsWriter = new GeneratorAssertsXPathLang(resultDirAssert);
		assertsWriter.writeAssertsToFile(assertList);


//		File assertsGeneratedFile = new File("src\\generated\\xsd-asserts\\Asserts.xpath");
//		File assertsValidationDir = new File("src\\generated", "AssertsValidator");
//		assertsValidationDir.mkdirs();
//		GeneratorXsdValidations generatorXsdValidations = GeneratorXsdValidations.getInstance();
//		generatorXsdValidations.runGenerator(assertsGeneratedFile.getAbsolutePath(), assertsValidationDir);


	}


}
