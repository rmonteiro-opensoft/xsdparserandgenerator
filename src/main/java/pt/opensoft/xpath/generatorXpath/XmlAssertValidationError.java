package pt.opensoft.xpath.generatorXpath;


public class XmlAssertValidationError {
	private String error;
	private String path;

	public XmlAssertValidationError(String error, String path) {
		this.error = error;
		this.path = path;
	}

	public String getError() {
		return error;
	}

	public String getPath() {
		return path;
	}

}