package pt.opensoft.xpath.gen;

import java.util.List;

public interface EList<E> extends List<E>
{
	/**
	 * Moves the object to the new position, if is in the list.
	 * @param newPosition the position of the object after the move.
	 * @param object the object to move.
	 */
	void move(int newPosition, E object);

	/**
	 * Moves the object from the old position to the new position.
	 * @param newPosition the position of the object after the move.
	 * @param oldPosition the position of the object before the move.
	 * @return the moved object.
	 */
	E move(int newPosition, int oldPosition);
}