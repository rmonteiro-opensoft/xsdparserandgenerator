/**
 * generated by Xtext 2.19.0
 */
package pt.opensoft.xpath.gen;





/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple For Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SimpleForClause#getVarname <em>Varname</em>}</li>
 *   <li>{@link SimpleForClause#getExprSingle <em>Expr Single</em>}</li>
 *   <li>{@link SimpleForClause#getVarname_list <em>Varname list</em>}</li>
 *   <li>{@link SimpleForClause#getExprSingle_list <em>Expr Single list</em>}</li>
 * </ul>
 *
 * @see XpathPackage#getSimpleForClause()
 * @model
 * @generated
 */
public interface SimpleForClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Varname</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Varname</em>' containment reference.
   * @see #setVarname(VarName)
   * @see XpathPackage#getSimpleForClause_Varname()
   * @model containment="true"
   * @generated
   */
  VarName getVarname();

  /**
   * Sets the value of the '{@link SimpleForClause#getVarname <em>Varname</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Varname</em>' containment reference.
   * @see #getVarname()
   * @generated
   */
  void setVarname(VarName value);

  /**
   * Returns the value of the '<em><b>Expr Single</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr Single</em>' containment reference.
   * @see #setExprSingle(ExprSingle)
   * @see XpathPackage#getSimpleForClause_ExprSingle()
   * @model containment="true"
   * @generated
   */
  ExprSingle getExprSingle();

  /**
   * Sets the value of the '{@link SimpleForClause#getExprSingle <em>Expr Single</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr Single</em>' containment reference.
   * @see #getExprSingle()
   * @generated
   */
  void setExprSingle(ExprSingle value);

  /**
   * Returns the value of the '<em><b>Varname list</b></em>' containment reference list.
   * The list contents are of type {@link VarName}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Varname list</em>' containment reference list.
   * @see XpathPackage#getSimpleForClause_Varname_list()
   * @model containment="true"
   * @generated
   */
  EList<VarName> getVarname_list();

  /**
   * Returns the value of the '<em><b>Expr Single list</b></em>' containment reference list.
   * The list contents are of type {@link ExprSingle}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr Single list</em>' containment reference list.
   * @see XpathPackage#getSimpleForClause_ExprSingle_list()
   * @model containment="true"
   * @generated
   */
  EList<ExprSingle> getExprSingle_list();

} // SimpleForClause
