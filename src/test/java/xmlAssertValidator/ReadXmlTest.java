package xmlAssertValidator;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import pt.opensoft.xpath.generatorXpath.XmlAssertValidationError;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

public class ReadXmlTest {

	private class PathAwareSaxAdapterValidator {

		private class EndElementException extends RuntimeException{
			public EndElementException(String message) {
				super(message);
			}
		}

		private Stack<String> pathStack;


		private XsdAssertsValidator validator;

		public PathAwareSaxAdapterValidator(XsdAssertsValidator validator) {
			this.pathStack = new Stack<>();
			this.validator = validator;
		}


		private String getPath(String localName) {
			if (pathStack.isEmpty()) {
				return localName;
			}
			return pathStack.peek() + "/" + localName;
		}


		public void startElement(String localName) {
			pathStack.push(getPath(localName));
			validator.startElement(pathStack.peek());
		}

		public List<XmlAssertValidationError> endElement(String localName, String xmlValue) {
			if (! Objects.equals(localName, pathStack.peek())) {
				throw new EndElementException("localName different than last element of pathStack");
			}
			List<XmlAssertValidationError> xsdAssertValidationErrors = validator.endElement(pathStack.peek(), xmlValue);
			xsdAssertValidationErrors.forEach(err -> {
				System.out.println(err.getPath() + " - " + err.getError());
			});

			pathStack.pop();

			return xsdAssertValidationErrors;
		}


	}


	public class SAXAdapter extends DefaultHandler {

		private Stack<String> stack;

		private PathAwareSaxAdapterValidator validator;
		private StringBuffer buffer = new StringBuffer();

		private String xmlValue;

		public SAXAdapter(XsdAssertsValidator xsdAssertsValidator) {
			this.stack = new Stack<>();
			this.validator = new PathAwareSaxAdapterValidator(xsdAssertsValidator);
		}

		@Override
		public void startDocument() throws SAXException {

		}

		@Override
		public void endDocument() throws SAXException {

		}


		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) {
			stack.push(localName);
			buffer.setLength(0);
			validator.startElement(localName);
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			xmlValue = buffer.toString();
			buffer.setLength(0);

			List<XmlAssertValidationError> xsdAssertValidationErrors = validator.endElement(localName, xmlValue);
			xsdAssertValidationErrors.forEach(err -> {
				System.out.println(err.getPath() + " - " + err.getError());
			});

			stack.pop();
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			buffer.append(ch, start, length);
		}

		@Override
		public void warning(SAXParseException e) throws SAXException {
			super.warning(e);
		}

		@Override
		public void error(SAXParseException e) throws SAXException {
			throw e;
		}

		@Override
		public void fatalError(SAXParseException e) throws SAXException {
			throw e;
		}

		@Override
		public void setDocumentLocator(Locator l) {
//			locator = l;
		}


	}

	private void test() throws MalformedURLException {
		Exception ex;
		File xsdFile = new File("src\\test\\resources\\saft.xsd");
		File xml = new File("src\\test\\resources\\xml\\saft-june.xml");
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(true);
			factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
			factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			factory.setFeature("http://apache.org/xml/features/xinclude", false);
			Schema schema = null;

			factory.setSchema(schema);
			SAXParser parser = factory.newSAXParser();
			InputStream xmlIs = new FileInputStream(xml);

			XsdAssertsValidator xsdAssertsValidator = new XsdAssertsValidator();

			SAXAdapter sa = new SAXAdapter(xsdAssertsValidator);
			parser.parse(xmlIs, sa);

		} catch (SAXException var7) {
			ex = var7.getException();

		} catch (IOException var8) {
			var8.printStackTrace();
		} catch (ParserConfigurationException var9) {
			var9.printStackTrace();
		}

	}


	public static void main(String[] args) {
		ReadXmlTest test = new ReadXmlTest();
		try {
			test.test();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}


}
