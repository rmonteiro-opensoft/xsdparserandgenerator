package xmlAssertValidator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.opensoft.xpath.generatorXpath.XmlAssertValidationError;

import java.util.List;

public class ValidationsTest {



	@Test
	public void testNegative(){

		XsdAssertsValidator xsdAssertsValidator = new XsdAssertsValidator();

		String currentPath = "SourceDocuments/SalesInvoices/Invoice";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/TotalPrice";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "100");

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/LineNumber";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.endElement(currentPath, null);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");


		currentPath = "SourceDocuments/SalesInvoices/Invoice";
		List<XmlAssertValidationError> errors = xsdAssertsValidator.endElement(currentPath, null);

		Assertions.assertNotNull(errors);
		Assertions.assertEquals(1, errors.size());
		Assertions.assertEquals("False", errors.get(0).getError());
		Assertions.assertEquals("SourceDocuments/SalesInvoices/Invoice", errors.get(0).getPath());
	}


	@Test
	public void testPositive(){

		XsdAssertsValidator xsdAssertsValidator = new XsdAssertsValidator();

		String currentPath = "SourceDocuments/SalesInvoices/Invoice";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/TotalPrice";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "400");

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/LineNumber";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.endElement(currentPath, null);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");


		currentPath = "SourceDocuments/SalesInvoices/Invoice";
		List<XmlAssertValidationError> errors = xsdAssertsValidator.endElement(currentPath, null);

		Assertions.assertNotNull(errors);
		Assertions.assertEquals(0, errors.size());
	}

}
