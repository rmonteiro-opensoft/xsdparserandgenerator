package xmlAssertValidator;

import pt.opensoft.xpath.generatorXpath.AssertValidation;
import pt.opensoft.xpath.generatorXpath.XmlAssertValidationError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 *
 * Código gerado para os Asserts:
 * SourceDocuments/SalesInvoices/Invoice/Line;
 * if ( ns:LineNumer ) then true() else false();
 *
 *
 * SourceDocuments/SalesInvoices/Invoice;
 * if ( sum(ns:Line/ns:UnitPrice) = ns:TotalPrice ) then true() else false();
 */
public class XsdAssertsValidator {

	Map<String, List<AssertValidation>> pathValidationsMap;
	List<AssertValidation> validationsInProgress;

	public XsdAssertsValidator() {
		pathValidationsMap = new HashMap<>();
		validationsInProgress = new LinkedList<>();
		init();
	}

	public void startElement(String path) {
		List<AssertValidation> assertValidations = pathValidationsMap.get(path);
		if(assertValidations != null) {
			validationsInProgress.addAll(assertValidations);
		}
	}

	public List<XmlAssertValidationError> endElement(String path, String value) {
		if (value != null) {
			validationsInProgress.forEach(assertValidation -> assertValidation.putValue(path, value));
		}

		List<AssertValidation> assertValidations = pathValidationsMap.get(path);
		if (assertValidations != null && ! assertValidations.isEmpty()) {
			List<XmlAssertValidationError> errors = validate(path, assertValidations);
			validationsInProgress.removeAll(assertValidations);
			return errors;
		}

		return Collections.emptyList();
	}

	private List<XmlAssertValidationError> validate(String path, List<AssertValidation> assertValidations) {
		List<XmlAssertValidationError> erros = new LinkedList<>();

		if (assertValidations != null) {
			assertValidations.forEach(val -> {
				Predicate<Map<String, String>> predicate = val.getPredicate();
				Map<String, String> xmlElementKeyAndValueMap = val.getXmlElementKeyAndValueMap();

				boolean isValid = predicate.test(xmlElementKeyAndValueMap);
				if (! isValid) {
					erros.add(new XmlAssertValidationError(val.getErrorMsg(), path));
				}

			});
		}

		return erros;
	}


	private void init() {
		List<AssertValidation> validationsList;
		validationsList = new ArrayList<>(1);
		validationsList.add(validateSourceDocumentsSalesInvoicesInvoice0());
		pathValidationsMap.put("AuditFile/SourceDocuments/SalesInvoices/Invoice", validationsList);

		validationsList = new ArrayList<>(1);
		validationsList.add(validateSourceDocumentsSalesInvoicesInvoiceLine0());
		pathValidationsMap.put("AuditFile/SourceDocuments/SalesInvoices/Invoice/Line", validationsList);

	}

	private AssertValidation validateSourceDocumentsSalesInvoicesInvoice0(){
		Predicate<Map<String, String>> predicate = map -> {
			String totalPrice = map.get("TotalPrice");
			String lineUnitPriceSum = map.get("Line/UnitPrice");
			if (Objects.equals(lineUnitPriceSum, totalPrice) ) {
				return true;
			}
			else {
				return false;
			}
		};


		Set<String> elementValuesToStore = Collections.emptySet();
		Set<String> elementValuesToSum = Collections.emptySet();
		Set<String> elementValuesToCount = Collections.emptySet();

		elementValuesToStore = Stream.of("TotalPrice").collect(Collectors.toSet());
		elementValuesToSum = Stream.of("Line/UnitPrice").collect(Collectors.toSet());

		return new AssertValidation("AuditFile/SourceDocuments/SalesInvoices/Invoice", elementValuesToStore, elementValuesToSum, elementValuesToCount, predicate, "False");
	}

	private AssertValidation validateSourceDocumentsSalesInvoicesInvoiceLine0(){
		Predicate<Map<String, String>> predicate = map -> {
			String lineNumer = map.get("LineNumer");
			if (lineNumer != null && ! lineNumer.isEmpty()) {
				return true;
			}
			else {
				return false;
			}
		};


		Set<String> elementValuesToStore = Collections.emptySet();
		Set<String> elementValuesToSum = Collections.emptySet();
		Set<String> elementValuesToCount = Collections.emptySet();

		elementValuesToStore = Stream.of("LineNumer").collect(Collectors.toSet());

		return new AssertValidation("AuditFile/SourceDocuments/SalesInvoices/Invoice/Line", elementValuesToStore, elementValuesToSum, elementValuesToCount, predicate, "False");
	}

}










